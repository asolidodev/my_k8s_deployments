# The kubernetes service deployment
resource "kubernetes_deployment" "deployment" {
  count = var.enable
  metadata {
    name      = var.name
    namespace = var.namespace
    labels = {
      app = var.name
    }
  }

  spec {
    replicas = var.replicas
    selector {
      match_labels = {
        app = var.name
      }
    }

    strategy {
      type = "RollingUpdate"

      rolling_update {
        max_surge       = 0
        max_unavailable = 1
      }
    }

    template {
      metadata {
        labels = {
          app = var.name
        }
      }

      spec {
        dynamic "host_aliases" {
          for_each = var.host_aliases_list
          content {
            ip        = host_aliases.value["ip"]
            hostnames = host_aliases.value["hostnames"]
          }
        }
        container {
          name    = var.name
          image   = var.image
          command = var.command
          resources {
            limits {
              memory = var.memory_limits
            }
            requests {
              memory = var.memory_requests
            }
          }
          # plain text vars (non-secrets)
          dynamic "env" {
            for_each = var.non_secret_vars_map
            content {
              name  = env.key
              value = env.value
            }
          }
          # encrypted vars (secrets)
          dynamic "env" {
            for_each = var.secret_vars_map
            content {
              name = env.key
              value_from {
                secret_key_ref {
                  name = var.secrets_name
                  key  = env.key
                }
              }
            }
          }
        }
      }
    }
  }
}

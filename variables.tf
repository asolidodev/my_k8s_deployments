variable "enable" {
  type = number
}

variable "name" {}

variable "namespace" {}

variable "image" {}

variable "command" {
  type = list
  default = []
}

variable "replicas" {
  type    = number
  default = 1
}

variable "memory_limits" {}

variable "memory_requests" {
  default = "100Mi"
}

variable "host_aliases_list" {
  type = list(object({ ip = string, hostnames = list(string) }))
  default = [
    {
      ip        = ""
      hostnames = []
    },
  ]
}

variable "non_secret_vars_map" {
  type = map(string)
  default = {}
}

variable "secrets_name" {}

variable "secret_vars_map" {
  type = map(string)
  default = {}
}

Usage:

eks-deployment.tf
```text
module "something_deployment" {
  source        = "git::ssh://git@bitbucket.org/asolidodev/my_k8s_deployments.git?ref=0.0.0"
  enable              = var.something_create ? 1 : 0
  name                = "something"
  namespace           = "something"
  image               = "something"
  memory_limits       = "100Mi"
  secrets_name        = "something"
  host_aliases_list   = var.host_aliases_list
  secret_vars_map     = var.secret_vars_map
  non_secret_vars_map = var.non_secret_vars_map
}

variable "host_aliases_list" {
  type = list(object({ ip = string, hostnames = list(string) }))
}

variable "non_secret_vars_map" {
  type = map(string)
}

variable "secret_vars_map" {
  type = map(string)
}
```

eks-deployment.tfvars
```text

variable "enable" {
  type = number
}
replicas = 3
memory_limits = "512Mi"
host_aliases_list = [
    {
        ip        = "123.123.123.123"
        hostnames = ["something-01"]
    },
    {
        ip        = "124.124.124.124"
        hostnames = ["something-02"]
    }
]
# encrypted vars
secret_vars_map = {
  SOMETHING = "SOMETHING ENCRYPTED"
  SOMETHING_ELSE = "SOMETHING ELSE ENCRYPTED"
}
# non-encrypted vars
non_secret_vars_map = {
  SOMETHING_EMAIL = "something@something.com"
  SOMETHING_URL   = "www.something.com"
}
```
